import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { HttpserviceService } from '../../httpservice.service';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

	@Output() dataCustomer = new EventEmitter <object>();
	
	myForm: FormGroup;
	isLogin: boolean = false;
	customers: any = [];

	constructor( private serviceHTTP : HttpserviceService) { }

	ngOnInit() {
		this.getCustomers();
	};

	submit(){
		localStorage.setItem('namePort','Login');
		this.serviceHTTP.httpPost("login",this.myForm.value).subscribe(
			data =>{
				localStorage.setItem('namePort','Show');
				this.getCustomers();
				this.isLogin = true;
			},
			error =>{
				console.log(error);
			}
		)
	};

	getCustomers(){
		this.customers = [];
		localStorage.setItem('namePort','Get');
		this.serviceHTTP.httpGet('customers').subscribe(
			data =>{
				this.customers = data;
			}, 
			error =>{
				console.log(error);
			}
		)	
	};

	editData(data){
		var dataAll: any  = {};
			dataAll['status'] = 'edit';
			dataAll['data'] = data;
		this.dataCustomer.emit(dataAll);
	};

	deleteData(data){
		console.log('from list '+ data);
		var dataAll: any  = {};
			dataAll['status'] = 'delete';
			dataAll['data'] = data;
		this.dataCustomer.emit(dataAll);
	};

}
