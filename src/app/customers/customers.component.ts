import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { HttpserviceService } from '../httpservice.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
	myForm: FormGroup;
	myFormManipulasi: FormGroup;
	isLogin: boolean = false;
	isAdd: boolean = true;
	status: string = "";
	id: string = "";

	@ViewChild(CustomerListComponent) private componentCustomer: CustomerListComponent;
  	
  	constructor(  private serviceHTTP: HttpserviceService) { }

  	ngOnInit() {
  		localStorage.removeItem('namePort');
  		this.newForm();
  	};

  	newForm(){
  		this.myForm = new FormGroup({
  			name: new FormControl(),
  			password: new FormControl()
  		});
  	};

  	newFormManipulasi(){
		this.myFormManipulasi = new FormGroup({
			name: new FormControl(),
			address: new FormControl(),
			telp: new FormControl()
		});
	};

	add(){
		this.newFormManipulasi();
		this.isAdd = false;
		this.status = "add";
	};

	submit(){
		localStorage.setItem('namePort','Post');
		this.serviceHTTP.httpPost("login", this.myForm.value).subscribe(
			data => {
				this.isLogin = true;
				this.newFormManipulasi();
			},
			error => {
				console.log(error);
			}
		)
	};

	save(){
		localStorage.setItem('namePort','Post');
		if (this.status == 'add'){
			this.serviceHTTP.httpPost("customers", this.myFormManipulasi.value).subscribe(
				data => {
					this.componentCustomer.getCustomers();
					this.newFormManipulasi();
				},
				error => {
					console.log(error);
				}
			)
		} else {
			this.serviceHTTP.httpPost("update/customers/"+this.id, this.myFormManipulasi.value).subscribe(
				data => {
					this.componentCustomer.getCustomers();
					this.newFormManipulasi();
				},
				error => {
					console.log(error);
				}
			)
		}
		this.isAdd = true;
	};
	
	deleteCustomers(id){
		localStorage.setItem('namePort','Delete');
		this.serviceHTTP.httpDelete("customers/"+id).subscribe(
			data => {
				this.componentCustomer.getCustomers();
				this.newFormManipulasi();
			},
			error => {
				console.log(error);
			}
		)
		this.isAdd = true;
	};

	receiveDataFromChild($event){
		if($event.status == 'edit'){
			this.myFormManipulasi.setValue({
				name: $event.data.name,
				address: $event.data.address,
				telp: $event.data.telp
			});
			this.isAdd = false;
			this.status = 'edit';
			this.id = $event.data._id;
		} else {
			this.deleteCustomers($event.data);
		};
	};
}
