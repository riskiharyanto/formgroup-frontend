import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersComponent } from './customers.component';
import { CustomerActionComponent } from './customer-action/customer-action.component';
const routes: Routes = [
	{
		path: '',
		component: CustomersComponent
	}
];

@NgModule({
  	imports: [RouterModule.forChild(routes)],
  	exports: [RouterModule]
})

export class CustomersRoutingModule { }
