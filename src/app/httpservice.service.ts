import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import * as _ from 'lodash';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpserviceService {

	url: any = [
		{
			name: 'Post',
			port: "http://localhost:3000/v1/"
		},
		{
			name: 'Get',
			port: "http://localhost:3000/v1/"
		},
    {
      name: 'Delete',
      port: "http://localhost:3000/v1/"
    }
	];
  	constructor(private http: Http) { }

  	httpPost(param, data: Object)/*: Observable<Comment[]>*/ {
  		let headers = new Headers({'Content-Type': 'application/json'});
  		let options = new RequestOptions({headers: headers});
  		let body = JSON.stringify(data);
  		var thisURL = localStorage.getItem('namePort');
      var thisURLSelected = this.getPort(thisURL);
      /*return this.http.post(thisURLSelected+param, body, options)*/
      return this.http.post(thisURLSelected+param, body, options)
        .map((response: Response)=> response.json())
        /*.catch((error: any) => Observable.throw(error.json().error || 'Server Error'))*/
  	};

  	httpGet(param)/*: Observable<Comment[]>*/{
  		var thisURL = localStorage.getItem('namePort');
      var thisURLSelected = this.getPort(thisURL);
      return this.http.get(thisURLSelected+param)
        .map((response:Response)=>response.json())
        /*.catch((error:any) => Observable.throw(error.json().error || 'Server Error') )*/
  	};

    httpDelete(param)/*: Observable<Comment[]>*/{
      var thisURL = localStorage.getItem('namePort');
      var thisURLSelected = this.getPort(thisURL);
      return this.http.delete(thisURLSelected+param)
        .map((response: Response)=>response.json())
        /*.catch((error:any) => Observable.throw(error.json().error || 'Server Error') )*/
    }

    getPort(param): string{
      var port: any = {};
        port = _.find(this.url, { 'name': param});
      return port.port;
    };
};
