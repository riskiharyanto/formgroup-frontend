import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule }   from '@angular/forms';
import { HttpserviceService } from './httpservice.service';

const appRoutes: Routes = [ //utk routing ke masing2 component
	{
		path: '', 
		redirectTo: '',
		pathMatch: 'full'
	},
	{
		path : 'customers', 
		loadChildren: 'app/customers/customers.module#CustomersModule'
	},
	{
		path : 'orders',
		loadChildren: 'app/orders/orders.module#OrdersModule'
	}
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [
  	HttpserviceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
